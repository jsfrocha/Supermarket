package pt.ulht.supermarket;

import java.io.Console;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import pt.ulht.beans.Cliente;
import pt.ulht.beans.Caixa;
import pt.ulht.utils.Helpers;
import pt.ulht.utils.DataLogs;

public class ModoManual {
	
	public static List<Caixa> listaCaixasManual = new ArrayList<Caixa>();
	public static List<Cliente> listaClientesManual = new ArrayList<Cliente>();
	
	
	//Criar novo cliente e adicion�-lo � caixa com menor fila
	public static void criarCliente() {
		
		//Gerar n� de produtos entre 2 e 120
		Random randomGen = new Random();
		Integer nProdutos = Helpers.generateRandomInteger(2, 120, randomGen);
		
		//Criar novo cliente
		Cliente cliente = new Cliente(Cliente.nClientes, nProdutos, false);
		
		//Adicionar cliente na lista local
		listaClientesManual.add(cliente);
		
		//Adicionar cliente � caixa com fila mais pequena
		System.out.println("Adiciona � fila mais pequena...");
		if (!listaCaixasManual.isEmpty()) {
			
			int tempSizeList = 0;
			int caixaMenorFilaIndex = 0;
			
			for (int i = 0; i < listaCaixasManual.size(); i++) {
				tempSizeList = listaCaixasManual.get(i).getnClientesFila();
				try {
					if (listaCaixasManual.get(i+1).getnClientesFila() < tempSizeList) {
						caixaMenorFilaIndex = i+1;
					} else {
						caixaMenorFilaIndex = i;
					}
				} catch (IndexOutOfBoundsException e) {
					
				}
			}

			Caixa caixaMenorFila = listaCaixasManual.get(caixaMenorFilaIndex);
			caixaMenorFila.inserirCliente(cliente);
			cliente.setCaixa(caixaMenorFila.getId());
			DataLogs.addClientToRegisterInXML(caixaMenorFila.getId().toString());
			System.out.println("Cliente ID "+cliente.getId()+" inserido na menor fila (Caixa ID "+caixaMenorFila.getId()+")");
		} else {
			System.out.println("N�o h� caixas dispon�veis, a criar caixa...");
			Caixa novaCaixa = criarCaixa();
			novaCaixa.inserirCliente(cliente);
			System.out.println("Cliente ID "+cliente.getId()+" inserido na fila da nova caixa ID "+novaCaixa.getId()+".");
		}
	}
		
	
	
	public static Caixa criarCaixa() {
		Caixa caixa = new Caixa(false);
		listaCaixasManual.add(caixa);
		return caixa;
	}
	
	public static void mostrarFilas() {
		/*if (!Supermarket.listaCaixasLog.isEmpty()) {
			for (int i = 0; i < Supermarket.listaCaixasLog.size(); i++) {
				if (!Supermarket.listaCaixasLog.contains(listaCaixasManual.get(i))) {
					listaCaixasManual.add(Supermarket.listaCaixasLog.get(i));
				}
			}
		}*/
			for (int i = 0; i < listaCaixasManual.size(); i++)  {
				System.out.println("------------------------");
				System.out.println("| ID: "+( listaCaixasManual.get(i).getId() )+"                |");
				System.out.println("| Fila: "+listaCaixasManual.get(i).getnClientesFila()+"              |");
				System.out.println("| Atend: "+listaCaixasManual.get(i).getnClientesAtendidos()+"             |");
				System.out.println("| Total: "+listaCaixasManual.get(i).getTempoTotalAtendimento()+"             |");
				System.out.println("| Medio: "+listaCaixasManual.get(i).getTempoMedioAtendimento()+"             |");
				System.out.println("------------------------");
				
				if (!listaCaixasManual.get(i).getListaClientes().isEmpty()) {
					LinkedList<Cliente> listaClientes = listaCaixasManual.get(i).getListaClientes();
					System.out.println("Fila de Clientes: ");
					for (Cliente cliente : listaClientes) {
						System.out.println("Cliente "+(cliente.getId() ));
						System.out.println("Numero de Produtos: "+cliente.getnProdutos());
						System.out.println("");
					}
				}
			}
		} 
	
	
	public static void retirarCaixas() { //Not Working
		if (!Supermarket.listaCaixasLog.isEmpty()) {
			for (int i = 0; i < Supermarket.listaCaixasLog.size(); i++) {
				if (!Supermarket.listaCaixasLog.contains(listaCaixasManual.get(i))) {
					listaCaixasManual.add(Supermarket.listaCaixasLog.get(i));
				}
			}
		}
		
		for (int i = 0; i < listaCaixasManual.size(); i++) { //For each Caixa in ListaCaixasManual
			System.out.println("N Caixas Manual (retirar): "+listaCaixasManual.size());
			System.out.println("I Value (retirar): "+i);
			if (listaCaixasManual.get(i).getnClientesFila() == 0) { //Se n�o tiver clientes em fila, remove-se da lista
				System.out.println("Caixa ID: "+listaCaixasManual.get(i).getId()+"n�o tem clientes em fila");
				listaCaixasManual.remove(i); // Ao remover 1�, 2� d� IndexOutOfBounds. - Poss�vel Solu��o listaCaixas - HashMap (Caixa, Cliente) e remove element if Cliente == Null
				System.out.println("retirarCaixas Caixa ID: "+listaCaixasManual.get(i).getId());
				DataLogs.deleteRegister(listaCaixasManual.get(i).getId().toString());
			}
		}
		
		
	}
	
	public static void atenderClientes() {
		/*for (Caixa caixa : listaCaixasManual) {
			
		}*/
	}
	
	
		
}
	
	
	

