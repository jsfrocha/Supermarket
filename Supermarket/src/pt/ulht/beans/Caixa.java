package pt.ulht.beans;

import java.util.LinkedList;
import pt.ulht.utils.DataLogs;


public class Caixa implements Runnable {
	
	public static Integer nCaixas = 0;
	
	Integer id;
	Integer nClientesFila;
	Integer nClientesAtendidos;
	Integer tempoTotalAtendimento;
	Integer tempoMedioAtendimento;
	LinkedList<Cliente> listaClientes = new LinkedList<Cliente>();
	
	public Caixa(boolean fromLog) { //Para novas Caixas
		this.id = nCaixas;
		this.nClientesFila = listaClientes.size();
		this.nClientesAtendidos = 0;
		this.tempoMedioAtendimento = 0;
		this.tempoTotalAtendimento = 0;
		
			DataLogs.writeNewRegister(this.id.toString(),
					  				  this.nClientesFila.toString(),
					  				  this.nClientesAtendidos.toString(),
					  				  this.tempoTotalAtendimento.toString(),
					  				  this.tempoMedioAtendimento.toString());
		
		nCaixas += 1;
	}
	
	public Caixa(Integer id, Integer nClientesFila, Integer nClientesAtendidos, Integer tempoMedioAtendimento, Integer tempoTotalAtendimento) { //Para caixas gravadas em Log
		this.id = id;
		this.nClientesFila = nClientesFila;
		this.nClientesAtendidos = nClientesAtendidos;
		this.tempoMedioAtendimento = tempoMedioAtendimento;
		this.tempoTotalAtendimento = tempoTotalAtendimento;
	}
	
	@Override
	public void run() {
				
	}
	
	//Getters and Setters
	
	
	//Insere cliente na fila da caixa
	public void inserirCliente(Cliente cliente) {
		if (!listaClientes.contains(cliente)) {
			listaClientes.add(cliente);
			this.nClientesFila = listaClientes.size();
			DataLogs.modifyRegister(this.id.toString(), "nFila", this.nClientesFila.toString());
		}
		else {
			System.out.println("Cliente j� est� na fila");
		}
	}

	public Integer getId() {
		return id;
	}

	public Integer getnClientesFila() {
		return nClientesFila;
	}

	public Integer getnClientesAtendidos() {
		return nClientesAtendidos;
	}

	public Integer getTempoTotalAtendimento() {
		return tempoTotalAtendimento;
	}

	public Integer getTempoMedioAtendimento() {
		return tempoMedioAtendimento;
	}
	
	public LinkedList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setnClientesFila(Integer nClientesFila) {
//		this.nClientesFila = nClientesFila;
	}

	public void setnClientesAtendidos(Integer nClientesAtendidos) {
		this.nClientesAtendidos = nClientesAtendidos;
	}

	public void setTempoTotalAtendimento(Integer tempoTotalAtendimento) {
		this.tempoTotalAtendimento = tempoTotalAtendimento;
	}

	public void setTempoMedioAtendimento(Integer tempoMedioAtendimento) {
		this.tempoMedioAtendimento = tempoMedioAtendimento;
	}
	
	public void setListaClientes(LinkedList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;

	}


	//End of Getters and Setters
	
}
