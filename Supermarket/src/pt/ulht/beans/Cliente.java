package pt.ulht.beans;

import pt.ulht.utils.DataLogs;

public class Cliente implements Runnable {
	
	public static Integer nClientes = 0;
	
	private Integer id;
	private Integer nProdutos;
	private Integer filaCaixaId;
	
	public Cliente(Integer id, Integer nProdutos, boolean fromLog) {
		this.id = nClientes;
		this.nProdutos = nProdutos;
		
		if (!fromLog) {	//Escrever novo cliente no Log
			DataLogs.writeNewClient(this.nProdutos.toString());
		}
		
		nClientes += 1;
	}
	
	//Getters and Setters
	public Integer getCaixa() {
		return filaCaixaId;
	}
	
	public void setCaixa(Integer filaCaixaId) {
		this.filaCaixaId = filaCaixaId;
	}
	public Integer getId() {
		return id;
	}

	public Integer getnProdutos() {
		return nProdutos;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setnProdutos(Integer nProdutos) {
		this.nProdutos = nProdutos;
	}

	@Override
	public void run() {

		
	}
	
	//End of Getters and Setters	
	
}
